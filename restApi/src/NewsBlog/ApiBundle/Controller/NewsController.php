<?php

namespace NewsBlog\ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View;
use NewsBlog\ApiBundle\Entity\News;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations\Get;

class NewsController extends FOSRestController
{
    /**
     * List action
     * @Route("/news", name="news_all")
     * @Method("GET")
     * @ApiDoc(
     *  resource=true,
     *      description="Return all news records",
     *      cache=false,
     *      section="News",
     *      statusCodes = {
     *          Response::HTTP_OK = "Returned when get news records",
     *          Response::HTTP_NOT_FOUND = "Returned when have no records in db"
     *      }
     * )
     */
    public function allAction()
    {
        $restresult = $this->getDoctrine()
            ->getRepository('NewsBlogApiBundle:News')
            ->findAll();

        if ($restresult === null) {
            return new View("there are no news exist", Response::HTTP_NOT_FOUND);
        }

        return new View($restresult, Response::HTTP_OK);
    }

    /**
     * Get action
     * @Rest\View
     * @Route("/news/{id}", name="news_get")
     * @Method("GET")
     * @ApiDoc(
     *  description="Search news record by id",
     *  section="News",
     *  resource=true,
     *  statusCodes = {
     *     Response::HTTP_OK = "Returned when get news record",
     *     Response::HTTP_NOT_FOUND = "Returns when the record can not be found"
     *  }
     * )
     */
    public function getAction($id)
    {
        $singleresult = $this->getDoctrine()
            ->getRepository('NewsBlogApiBundle:News')
            ->find($id);

        if ($singleresult === null) {
            return new View("News record not found", Response::HTTP_NOT_FOUND);
        }

        return new View($singleresult, Response::HTTP_OK);
    }

    /**
     * Add new news record
     * @Route("/news_new", name="news_new")
     * @Method("POST")
     * @ApiDoc(
     *  resource=true,
     *  description="Add new news record",
     *  cache=false,
     *  section="News",
     *  statusCodes = {
     *     Response::HTTP_OK = "Returned when new record written",
     *     Response::HTTP_NOT_ACCEPTABLE = "Returned when trying to write null values"
     *  }
     * )
     */
    public function newAction(Request $request)
    {
        $data = new News();
        $title = $request->get('title');
        $content = $request->get('content');
        $isapproved = $request->get('isapproved');

        if (empty($title) || empty($content) || empty($isapproved)) {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }

        $data->setTitle($title);
        $data->setContent($content);

        $datetime = new \DateTime();
        $datetime->format('Y-m-d H:i:s');

        $data->setDate($datetime);
        $data->setIsapproved($isapproved);

        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();

        return new View("News Added Successfully", Response::HTTP_OK);
    }

    /**
     * Edit news record
     * @Route("/news_edit/{id}", name="news_edit")
     * @Method("PUT")
     * @ApiDoc(
     *  resource=true,
     *  description="Edit news record",
     *  cache=false,
     *  section="News",
     *  statusCodes = {
     *     Response::HTTP_OK = "Returned when news record successfully updated",
     *     Response::HTTP_NOT_FOUND = "Returned when record can not be found"
     *  },
     * )
     */
    public function editAction($id,Request $request)
    {
        $data = new News();

        $title = $request->get('title');
        $content = $request->get('content');
        $isapproved = $request->get('isapproved');

        $sn = $this->getDoctrine()->getManager();
        $news = $this->getDoctrine()->getRepository('NewsBlogApiBundle:News')->find($id);

        if (empty($news)) {
            return new View("News record not found", Response::HTTP_NOT_FOUND);
        }
        elseif (!empty($title) && !empty($content) && !empty($isapproved)){
            $news->setTitle($title);
            $news->setContent($content);
            $news->setIsapproved($isapproved);
            $sn->flush();
            return new View("News record Updated Successfully", Response::HTTP_OK);
        }
        elseif (!empty($title) && empty($content) && empty($isapproved)){
            $news->setTitle($title);
            $sn->flush();
            return new View("Title Updated Successfully", Response::HTTP_OK);
        }
        elseif (empty($title) && !empty($content) && empty($isapproved)){
            $news->setContent($content);
            $sn->flush();
            return new View("Content Updated Successfully", Response::HTTP_OK);
        }
        elseif (empty($title) && empty($content) && !empty($isapproved)){
            $news->setIsapproved($isapproved);
            $sn->flush();
            return new View("Isapproved status Updated Successfully", Response::HTTP_OK);
        }
        else return new View("cannot be empty", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * Delete new record
     * @Route("/news_delete/{id}", name="news_delete")
     * @Method("DELETE")
     * @ApiDoc(
     *  resource=true,
     *  description="Delete new record",
     *  cache=false,
     *  section="News",
     *  statusCodes = {
     *     Response::HTTP_OK = "Returned when news record successfully deleted",
     *     Response::HTTP_NOT_ACCEPTABLE = "Returned when record can not be found"
     *  },
     * )
     */
    public function removeAction($id)
    {
        $data = new News();
        $sn = $this->getDoctrine()->getManager();
        $news = $this->getDoctrine()->getRepository('NewsBlogApiBundle:News')->find($id);
        if (empty($news)) {
            return new View("News record not found", Response::HTTP_NOT_FOUND);
        }
        else {
            $sn->remove($news);
            $sn->flush();
        }
        return new View("deleted successfully", Response::HTTP_OK);
    }

}
