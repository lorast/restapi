<?php

namespace NewsBlog\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;

class DemoController extends FOSRestController
{
    public function getDemosAction()
    {

        $data = array("hello" => "world");
        $view = $this->view($data);
        return $this->handleView($view);
    }

}
