<?php

namespace NewsBlog\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NewsBlogApiBundle:Default:index.html.twig');
    }
}
