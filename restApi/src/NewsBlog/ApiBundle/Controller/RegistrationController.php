<?php

namespace NewsBlog\ApiBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\Form\FormInterface;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\NelmioApiDocBundle;

class RegistrationController extends BaseController
{
    /**
     * Register new user
     * @Route("/register", name="users_register")
     * @Method("POST")
     * @ApiDoc(
     *  resource=true,
     *     description="Register new user",
     *     section="User",
     *     statusCodes={
     *          Response::HTTP_OK = "Returned when successfully added new user"
     *     }
     * )
     * @param Request $request
     * @return mixed
     */
    public function registerAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_provider.username');
        $entityManager = $this->get('doctrine')->getManager();
//        $data = $request->request->all();

        // Do a check for existing user with userManager->findByUsername

        $user = $userManager->createUser();
        $user->setUsername($request->get('username'));
        // ...
        $user->setPlainPassword($request->get('pass'));
        $user->setEmail($request->get('email'));
        $user->setEnabled(true);

        $userManager->updateUser($user);

        return $this->generateToken($user, 201);
    }

    protected function generateToken($user, $statusCode = 200)
    {
        // Generate the token
        $token = $this->get('lexik_jwt_authentication.jwt_manager')->create($user);

    $response = array(
        'token' => $token,
        'user'  => $user // Assuming $user is serialized, else you can call getters manually
    );

    return new JsonResponse($response, $statusCode); // Return a 201 Created with the JWT.
}

    /**
     * @param  Request $request
     * @param  FormInterface $form
     */
    private function processForm(Request $request, FormInterface $form)
    {
        $data = json_decode($request->getContent(), true);
        if ($data === null) {
            throw new BadRequestHttpException();
        }

        $form->submit($data);
    }

    /**
     * Data serializing via JMS serializer.
     *
     * @param mixed $data
     *
     * @return string JSON string
     */
    private function serialize($data)
    {
        $context = new SerializationContext();
        $context->setSerializeNull(true);

        return $this->get('jms_serializer')
            ->serialize($data, 'json', $context);
    }

    /**
     * Set base HTTP headers.
     *
     * @param Response $response
     *
     * @return Response
     */
    private function setBaseHeaders(Response $response)
    {
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }
}