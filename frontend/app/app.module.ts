import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NewsComponent} from "./news.component";
import { HttpModule } from "@angular/http";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule
    ],
    declarations: [
        AppComponent,
        NewsComponent
    ],
    bootstrap: [
        AppComponent,
        NewsComponent
    ]
})
export class AppModule {

}