import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';
import { NewsComponent } from "./news.component";

const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule, NewsComponent);
