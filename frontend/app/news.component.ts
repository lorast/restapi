import { Component } from '@angular/core';

import { NewsService } from './shared/news.service';

@Component({
    moduleId: module.id,
    providers: [NewsService],
    selector: 'news-box',
    templateUrl: 'news.component.html'
})
export class NewsComponent {
  constructor(private newsService: NewsService) {
    this.loadNews()
  }

  news = Array();

  loadNews() {
    this.newsService.getNews().subscribe(data => this.news = [data]);
  }
}